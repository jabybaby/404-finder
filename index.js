const domain = "example.com"
const axios = require("axios");

let urls = [{url:"https://"+domain+"/", level:0}]
let done = []
let errored = [];
const regex = new RegExp("(http|ftp|https):\\/\\/(www\\."+domain.replace(".","\\.")+"|"+domain.replace(".","\\.")+")([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])","gm")

async function main(){
  while(urls.length>0)
  {
    let failed = false
    var act = urls.shift();
    console.log(act)
    var j = await axios.get(act.url).catch(e => {failed = true})
    if(failed){
      errored.push(act)
      console.log("Unknown error: "+act.url)
    }
    else if(typeof j.data!=typeof "f"){
      console.log("Not text: "+act.url)
    }
    else if(j.status != 200){
      errored.push(act)
      console.log("Error "+j.status+": "+act.url)
    }
    else{
      done.push(act)
      var kk = [... new Set(Array.from(j.data.matchAll(regex), m => m[0]).map(el => el.split("#")[0]))]
      kk.forEach((item, i) => {
        if(!urls.map(el => el.url).includes(item) && !done.map(el => el.url).includes(item))
        urls.push({url: item, level: act.level+1})
      });
    }
  }
  console.log(done)
  console.log(errored.map(el => el.url))
}

main()